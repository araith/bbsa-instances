Here is a description of problem instances used in bi-objective Benders simplex paper. In the following, it is described how instances we tested are generated. 


Here is some definitions for variables used:


C = [c1 ; c2] , where the c1 and c2 correspond to the unit costs of the first and the second objectives, respectively.

F = [f1 ; f2], where the f1 and f2 correspond to the fixed costs of the first and the second objectives, respectively.

A = The constraint coefficients corresponding to x variables in sub-problem.

B = The constraint coefficients corresponding to y variables in sub-problem.

b = RHS of the subproblem constraints.

D_m = The constraints corresponding to the master problem.

d = Right hand side (RHS) of the master constraints 


The general form of a bi-objective fixed charge transportation problem is as follows:
	
	min c^{1} x + f^{1} y
	min c^{2} x + f^{2} y
	s.t A     x + B     y <= b,
	    0     x   D_m   y <= d,
			  x	        y >= 0.  

The instances generated in a form of 

	F=[f^{1};F^{2}];  
		 
	C = [c^{1};
	     c^{2}];
   
    D=[0 D_m];
	
	A is a matrix of x variables in BS
	B is a matrix of y variables in BS


BiFCTP / Bi-objective fixed charge transportation problem:

Fixed Charge Transportation Problem data from the literature, adapted to include a second objective function. Data_set_1 and Data_set_2 instances are chosen from [1], where Data_set_3 instances are from [2]. Since these two papers address a single-objective linear fixed charge transportation problem, we add a second objective function for unit and fixed costs. To generate the second objective, we find the first objective's min and max values for unit cost (c_min, c_max) and fixed cost (name f_min, f_max). Then we generate values for unit cost and fixed costs randomly in the interval [c_min, c_max], [f_min,f_max] with a uniform distribution. 

BiMKP / Bi-objective multiple knapsack problem:

Bi-objective multiple knapsack problem (BiMKP) instances are taken originally from https://www2.isye.gatech.edu/~sahmed/siplib/smkp/smkp.html [3], where all benchmarks are based on single-objective optimisation problem. We generate the second objective function with uniform distribution. This folder contains 30 different instances of BiMKP.

BiPP / Bi-objective portfolio problem:

Bi-objective portfolio problem (BiPP) instances are taken originally from https://www2.isye.gatech.edu/~sahmed/siplib/probport/probport.html [3], where all benchmarks are based on single-objective optimisation problem. We generate the second objective function with uniform distribution. This folder contains 100 different instances of BiPP.  

References:

[1] Agarwal, Y., & Aneja, Y. (2012). Fixed-charge transportation problem: Facets of the projection polyhedron. Operations research, 60(3), 638-654.

[2] Roberti, R., Bartolini, E., & Mingozzi, A. (2015). The fixed charge transportation problem: An exact algorithm based on a new integer programming formulation. Management Science, 61(6), 1275-1291.

[3] Ahmed, S., Garcia, R., Kong, N., Ntaimo, L., Parija, G., Qiu, F., Sen, S., 2015. Siplib: A stochastic integer programming test problem library. URL: \url{https://www2.isye.gatech.edu/~sahmed/siplib}
